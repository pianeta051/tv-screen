module EpisodesHelper
  def number_episodes(season)
    Episode.where(season:season).count
  end
end
