# == Schema Information
#
# Table name: episodes
#
#  id         :integer          not null, primary key
#  season     :integer
#  number     :integer
#  name       :string
#  duration   :integer
#  synopsis   :text
#  image      :string
#  link       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :episode do
    season { 1 }
    number { 1 }
    name { "MyString" }
    duration { 1 }
    synopsis { "MyText" }
    image { "MyString" }
    link { "MyString" }
  end
end
