require 'rails_helper'

RSpec.describe "episodes/new", type: :view do
  before(:each) do
    assign(:episode, Episode.new(
      :season => 1,
      :number => 1,
      :name => "MyString",
      :duration => 1,
      :synopsis => "MyText",
      :image => "MyString",
      :link => "MyString"
    ))
  end

  it "renders new episode form" do
    render

    assert_select "form[action=?][method=?]", episodes_path, "post" do

      assert_select "input[name=?]", "episode[season]"

      assert_select "input[name=?]", "episode[number]"

      assert_select "input[name=?]", "episode[name]"

      assert_select "input[name=?]", "episode[duration]"

      assert_select "textarea[name=?]", "episode[synopsis]"

      assert_select "input[name=?]", "episode[image]"

      assert_select "input[name=?]", "episode[link]"
    end
  end
end
