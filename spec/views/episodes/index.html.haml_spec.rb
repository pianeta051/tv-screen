require 'rails_helper'

RSpec.describe "episodes/index", type: :view do
  before(:each) do
    assign(:episodes, [
      Episode.create!(
        :season => 2,
        :number => 3,
        :name => "Name",
        :duration => 4,
        :synopsis => "MyText",
        :image => "Image",
        :link => "Link"
      ),
      Episode.create!(
        :season => 2,
        :number => 3,
        :name => "Name",
        :duration => 4,
        :synopsis => "MyText",
        :image => "Image",
        :link => "Link"
      )
    ])
  end

  it "renders a list of episodes" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Image".to_s, :count => 2
    assert_select "tr>td", :text => "Link".to_s, :count => 2
  end
end
