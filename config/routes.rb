Rails.application.routes.draw do
  devise_for :users, controllers: {
     registrations: 'users/registrations'
   }


   as :user do
     get 'sign-up', to: 'devise/registrations#new', as: :sign_up
     get 'sign-in', to: 'devise/sessions#new', as: :sign_in
     delete 'sign_out', to: 'devise/sessions#destroy', as: :sign_out
     get 'profile', to: 'devise/registrations#edit', as: :profile
   end

  resources :episodes
  get '/seasons', to: 'episodes#seasons', as: :seasons
  get '/season/:number', to: 'episodes#season', as: :season
  get '/last-episode', to: 'episodes#last', as: :last_episode
  get '/search', to: 'episodes#search', as: :search
  get '/ebbasons', to: 'episode#ebbasons', as: :ebbasons
  root to: 'episodes#index'
end
